import unittest
from unittest.mock import Mock

from memory_address_traces.analyses import sequential_pattern, Subsequence, time_pattern, spatial_pattern, \
    OverlappingIntervals, Range, ProblematicAddresses, StronglyConnectedComponents, TraceGraphCompression
from memory_address_traces.data_structures import PinatraceInput


class TestPatterns(unittest.TestCase):
    def setUp(self):
        self.pinatrace_input = PinatraceInput("test/input_files/pinatrace.out")
        self.pinatrace_input2 = PinatraceInput("test/input_files/pinatrace3.out")

    def test_sequential_pattern_false_skip(self):
        subsequences = sequential_pattern(self.pinatrace_input, 2, False)
        self.assertCountEqual([Subsequence(length=3, end=5)], subsequences)

    def test_sequential_pattern_true_skip(self):
        subsequences = sequential_pattern(self.pinatrace_input2, 2, True)
        self.assertCountEqual([Subsequence(length=5, end=4), Subsequence(length=5, end=10)], subsequences)

    def test_spatial_pattern_false_skip(self):
        subsequences = spatial_pattern(self.pinatrace_input, 3, False, 20)
        self.assertCountEqual([Subsequence(length=6, end=5)], subsequences)

    def test_time_pattern(self):
        pattern = time_pattern(self.pinatrace_input)
        self.assertEqual({"7ffc74bda230": [1], "7ffc74bda221": [2, 3, 9], "7ffc74bda222": [4], "7ffc74bda223": [5],
                          "7ffc74bda238": [0], "7ffc74bda208": [6], "7ffc74bda224": [7, 8]}, pattern)


class TestOverlappingIntervals(unittest.TestCase):
    def setUp(self):
        self.pinatrace_input = PinatraceInput("test/input_files/pinatrace4.out")

    def test_overlapping_intervals(self):
        overlapping_intervals = OverlappingIntervals(self.pinatrace_input)
        overlapping_intervals.compute_num_of_overlapping_intervals()
        self.assertEqual(4, overlapping_intervals.max_num_of_overlapping_intervals)
        self.assertEqual({'0': Range(start=0, end=1), '1': Range(start=1, end=9), '2': Range(start=2, end=7),
                          '3': Range(start=3, end=8), '4': Range(start=4, end=5), '99': Range(start=9, end=10)},
                         overlapping_intervals.live_ranges)


class TestProblematicAddresses(unittest.TestCase):
    def setUp(self):
        self.pinatrace_input = PinatraceInput("test/input_files/pinatrace5.out")

    def test_problematic_addresses(self):
        problematic_addresses = ProblematicAddresses(self.pinatrace_input)
        self.assertCountEqual(['1'], problematic_addresses.only_read_from)
        self.assertCountEqual(['4', '5', '9'], problematic_addresses.only_written_to)
        self.assertCountEqual(['8'], problematic_addresses.read_before_write)


class TestStronglyConnectedComponents(unittest.TestCase):
    def setUp(self):
        self.trace_graph_mock = Mock()
        self.trace_graph_mock.num_of_vertices = 5
        self.trace_graph_mock.adjacency = [{2: 1, 3: 1}, {0: 1}, {1: 1}, {4: 1}, {}]

    def test_strongly_connected_components(self):
        strongly_connected_components = StronglyConnectedComponents(self.trace_graph_mock)
        strongly_connected_components.compute_strongly_connected_components()
        sorted_strongly_connected_components = [sorted(x) for x in
                                                strongly_connected_components.strongly_connected_components]
        self.assertCountEqual([[0, 1, 2], [3], [4]], sorted_strongly_connected_components)


class TestTraceGraphCompression(unittest.TestCase):
    def setUp(self):
        self.trace_graph_mock = Mock()
        self.trace_graph_mock.num_of_vertices = 6
        self.trace_graph_mock.first_vertex_int = 0
        self.trace_graph_mock.adjacency = [{1: 1, 2: 1}, {2: 1}, {3: 1}, {4: 1}, {5: 1, 3: 2}, {}]

        self.trace_graph_mock2 = Mock()
        self.trace_graph_mock2.num_of_vertices = 6
        self.trace_graph_mock2.first_vertex_int = 0
        self.trace_graph_mock2.adjacency = [{1: 1, 2: 1}, {2: 1}, {3: 1}, {4: 1}, {}, {}]

    def test_trace_graph_compression_compressed_vertices(self):
        trace_graph_compression = TraceGraphCompression(self.trace_graph_mock)
        trace_graph_compression.compress_graph()
        self.assertEqual({2, 3, 4}, trace_graph_compression.compressed_vertices)
        self.assertEqual(3, trace_graph_compression.num_of_compressed_vertices)
        self.trace_graph_mock.add_edge.assert_called_with(2, 4, 1)

        trace_graph_compression2 = TraceGraphCompression(self.trace_graph_mock2)
        trace_graph_compression2.compress_graph()
        self.assertEqual({2, 3, 4}, trace_graph_compression2.compressed_vertices)
        self.assertEqual(3, trace_graph_compression2.num_of_compressed_vertices)
        self.trace_graph_mock.add_edge.assert_called_with(2, 4, 1)
