import sys

from memory_address_traces.analyses import spatial_pattern, TraceGraphCompression, ProblematicAddresses
from memory_address_traces.data_structures import PinatraceInput, TraceGraph
from memory_address_traces.results_visualisations import plot_addresses, percentage_of_addresses_needed, \
    plot_percentage_of_addresses_needed


def plot_addresses_example():
    # run with Python 3.9.

    # choose class from data_structures which represents file you want to analyse
    pinatrace_input = PinatraceInput("/path_to_file/pinatrace.out")
    # itrace_input = ITraceInput("/path_to_file/itrace.out")
    # memtrace_input = MemtraceInput("/path_to_file/memtrace.log")
    # instrace_input = InstraceInput("/path_to_file/instrace.log")

    # list of tuples (length of subsequence, last index of subsequence)
    subsequences = spatial_pattern(pinatrace_input, min_subsequence_length=5, skip=True, neighbourhood=5)

    plot_addresses(subsequences, pinatrace_input)


def percentage_of_addresses_needed_example():
    pinatrace_input = PinatraceInput("/path_to_file/pinatrace.out")

    print(percentage_of_addresses_needed(pinatrace_input))


def plot_percentage_of_addresses_needed_example():
    pinatrace_input = PinatraceInput("/path_to_file/pinatrace.out")

    print(plot_percentage_of_addresses_needed(pinatrace_input))


# type (only read from, only written to, read before written)
def addresses_by_type():
    pinatrace_input = PinatraceInput("/path_to_file/pinatrace.out")
    problematic_addresses = ProblematicAddresses(pinatrace_input)

    print(f"only read from: {len(problematic_addresses.only_read_from)}",
          f"only written to: {len(problematic_addresses.only_written_to)}",
          f"read before write: {len(problematic_addresses.read_before_write)}",
          f"all addresses: {pinatrace_input.num_of_addresses}")


def trace_graph_compression_example():
    # run with PyPy
    sys.setrecursionlimit(15000)

    pinatrace_input = PinatraceInput("/path_to_file/pinatrace.out")

    trace_graph = TraceGraph(pinatrace_input)
    compression = TraceGraphCompression(trace_graph)

    compression.compress_graph()

    print(f"Compressed {(compression.num_of_compressed_vertices / trace_graph.num_of_vertices) * 100}% of vertices.")
