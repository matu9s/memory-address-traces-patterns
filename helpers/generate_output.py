import os
import sys


# function returns all output files in directory generated with pinatrace_output
def all_input_files_paths(dir_path):
    sub_dirs = ['', 'cpp_dir', 'py_dir']
    all_input_files_paths = list()
    for dir in sub_dirs:
        for file in os.listdir(os.fsencode(dir_path + f'/{dir}/')):
            filename = os.fsdecode(file)
            if '.out' not in filename:
                continue
            all_input_files_paths.append(dir_path + f'/{dir}/' + filename)
    return all_input_files_paths


# script used to generate multiple pinatrace files
# in output directory must be created directories py_dir and cpp_dir
def pinatrace_output(file_name):
    with open(file_name, "r") as input_file:
        content = input_file.read()
        output_file_dir = content.split('\n', 1)[0]
        commands_list = content.splitlines()[1:]
        commands_list = commands_list[1:]
    directories = ['', 'py_dir/', 'cpp_dir/']
    for command in commands_list:
        output_file_subdir = ''
        for directory in directories:
            if directory in command:
                output_file_subdir = directory
        output_file_name = command.replace(' ', '_').replace(output_file_subdir, '').replace('/', '~f-slash~') + '.out'
        if not os.path.isfile(
                output_file_dir + output_file_subdir + output_file_name):
            print('../../../pin -t obj-intel64/pinatrace.so -- ' + command)
            os.system('../../../pin -t obj-intel64/pinatrace.so -- ' + command)
            os.rename(output_file_dir + 'pinatrace.out', output_file_dir + output_file_subdir + output_file_name)


if __name__ == '__main__':
    input_file_name = sys.argv[1]
    pinatrace_output(input_file_name)
