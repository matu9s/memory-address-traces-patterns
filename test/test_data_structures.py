import unittest

from memory_address_traces.data_structures import PinatraceInput, TraceGraph, ITraceInput, InstraceInput, MemtraceInput


class TestTraceInput(unittest.TestCase):
    def test_pinatrace_input(self):
        pinatrace_input = PinatraceInput("test/input_files/pinatrace2.out")
        self.assertEqual(
            ['7ffc74bda238', '7ffc74bda235', '7ffc74bda221', '7ffc74bda221', '7ffc74bda238', '7ffc74bda230'],
            pinatrace_input.addresses)
        self.assertEqual(4, pinatrace_input.num_of_addresses)
        self.assertEqual('R', pinatrace_input.lines[2].operation_type)
        self.assertEqual('7ffc74bda221', pinatrace_input.lines[2].address)

    def test_i_trace_input(self):
        itrace_input = ITraceInput("test/input_files/itrace.out")
        self.assertEqual(
            ['7f7c6e9fe0d0', '7f7c6e9fe0d3', '7f7c6e9fee30', '7f7c6e9fee34', '7f7c6e9fee35', '7f7c6e9fee38'],
            itrace_input.addresses)

    def test_memtrace_input(self):
        memtrace_input = MemtraceInput("test/input_files/memtrace.log")
        self.assertEqual(['b6da9a30', 'b6da9acc', 'b6da9a34', 'b6daa34c', 'bef1654c'], memtrace_input.addresses)

    def test_instrace_input(self):
        instrace_input = InstraceInput("test/input_files/instrace.log")
        self.assertEqual(['b6e18a30', 'b6e18a38', 'b6e18a3c', 'b6e19340', 'b6e19344', 'b6e1934c'],
                         instrace_input.addresses)


class TestTraceGraph(unittest.TestCase):
    def setUp(self) -> None:
        self.pinatrace_input = PinatraceInput("test/input_files/pinatrace2.out")

    def test_graph(self):
        trace_graph = TraceGraph(self.pinatrace_input)
        self.assertTrue(trace_graph.exists_edge(trace_graph.address_to_int["7ffc74bda238"],
                                                trace_graph.address_to_int["7ffc74bda235"]))
        self.assertTrue(trace_graph.exists_edge(trace_graph.address_to_int["7ffc74bda235"],
                                                trace_graph.address_to_int["7ffc74bda221"]))
        self.assertTrue(trace_graph.exists_edge(trace_graph.address_to_int["7ffc74bda221"],
                                                trace_graph.address_to_int["7ffc74bda221"]))
        self.assertTrue(trace_graph.exists_edge(trace_graph.address_to_int["7ffc74bda221"],
                                                trace_graph.address_to_int["7ffc74bda238"]))
        self.assertFalse(trace_graph.exists_edge(trace_graph.address_to_int["7ffc74bda221"],
                                                 trace_graph.address_to_int["7ffc74bda230"]))

        self.assertEqual(trace_graph.num_of_vertices, 4)

        self.assertEqual(trace_graph.int_to_address[trace_graph.first_vertex_int], "7ffc74bda238")
