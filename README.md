# Detekcia vzorov správania procesu v postupnosti adries
Bakalárska práca
## Požiadavky na spustenie
Program bol testovaný v Pythone 3.9 a PyPy 3.11

Potrebné knižnice sú v súbore [requirements.txt](requirements.txt)

Testy je možné spustiť pomocou `python -m unittest` z priečinka memory-address-traces-patterns



## Popis súborov
Program je rozdelený do súborov [data_structures.py](memory_address_traces/data_structures.py), [analyses.py](memory_address_traces/analyses.py), 
[results_visualisations.py](memory_address_traces/analyses.py)
* V súbore [data_structures.py](memory_address_traces/data_structures.py) sa nachádzajú dátové štruktúry používané v programe. Sú tu 
štruktúry reprezentujúce vstupné súbory zoznamov pristupovaných adries vytvorené nástrojmi Pin a Dynamorio a grafová štruktúra 
postupnosti pristupovaných adries.
* V súbore [analyses.py](memory_address_traces/analyses.py) sú implementované jednotlivé nástroje určené na analyzovanie pristupovaných adries.
* V súbore [results_visualisations.py](memory_address_traces/results_visualisations.py) sú pomocné funkcie, ktoré vizualizujú alebo vypisujú výsledky 
získané pomocou nástrojov.
* Ukážkový program vysvetľujúci základné použitie sa nachádza v [example.py](example.py)
* Príklady niektorých výsledkov a vizualizácií je možné nájsť v priečinku [results_examples](results_examples)