from collections import defaultdict, namedtuple
from dataclasses import dataclass
from enum import Enum, auto

from .data_structures import TraceGraph, PinatraceInput, TraceInput

Subsequence = namedtuple('Subsequence', 'length end')


# find subsequences based on pattern_formula
def _universal_spatial_pattern(trace_input: TraceInput, pattern_formula,
                               min_subsequence_length=1, skip=False, neighbourhood=None):
    continuous_subsequences = list()
    previous_address = None
    subsequence_length = 0
    skipped = False
    address_index = 0

    while address_index < len(trace_input.addresses):
        current_address = int(trace_input.addresses[address_index], 16)
        if previous_address is not None:
            if pattern_formula(previous_address, current_address, neighbourhood):
                subsequence_length += 1

            elif skip and not skipped and address_index + 1 < len(trace_input.addresses) and pattern_formula(
                    previous_address, int(trace_input.addresses[address_index + 1], 16), neighbourhood):
                subsequence_length += 1
                address_index += 1
                skipped = True
                continue

            elif subsequence_length >= min_subsequence_length:
                continuous_subsequences.append(Subsequence(subsequence_length + 1, address_index - 1))
                subsequence_length = 0
                skipped = False
            else:
                subsequence_length = 0
                skipped = False

        previous_address = current_address
        address_index += 1

    if subsequence_length >= min_subsequence_length:
        continuous_subsequences.append(Subsequence(subsequence_length + 1, address_index - 1))

    return sorted(continuous_subsequences, reverse=True)


# sequential pattern formula used in _universal_spatial_pattern function
def _sequential_formula(previous_address, current_address, neighbourhood):
    return previous_address == current_address - 1


# find subsequences with sequential pattern
def sequential_pattern(trace_input: TraceInput, min_subsequence_length, skip):
    return _universal_spatial_pattern(trace_input, _sequential_formula, min_subsequence_length, skip)


# spatial pattern formula used in _universal_spatial_pattern function
def _spatial_formula(previous_address, current_address, neighbourhood=1):
    return previous_address - neighbourhood <= current_address <= previous_address + neighbourhood


# find subsequences with spatial pattern
def spatial_pattern(trace_input: TraceInput, min_subsequence_length, skip, neighbourhood):
    return _universal_spatial_pattern(trace_input, _spatial_formula,
                                      min_subsequence_length, skip, neighbourhood)


# returns addresses and number of accesses to that address
def time_pattern(trace_input: TraceInput):
    address_occurrences = defaultdict(list)
    for index, address in enumerate(trace_input.addresses):
        address_occurrences[address].append(index)
    return dict(address_occurrences)


@dataclass
class Range:
    start: int
    end: int


# class used to compute percentage of addresses needed
class OverlappingIntervals:
    def __init__(self, trace_input: TraceInput):
        self._trace_input = trace_input
        self.live_ranges = dict()
        self._compute_live_ranges()
        self.interval_boundaries = list()

        Boundary = namedtuple('Boundary', 'boundary_index is_start')
        for live_range in self.live_ranges.values():
            self.interval_boundaries.append(Boundary(live_range.start, True))
            self.interval_boundaries.append(Boundary(live_range.end, False))
        self.interval_boundaries.sort()

        self.max_num_of_overlapping_intervals = 0

    # find live interval for each address
    def _compute_live_ranges(self):
        for index, address in enumerate(self._trace_input.addresses):
            if address in self.live_ranges:
                self.live_ranges[address].end = index + 1
            else:
                self.live_ranges[address] = Range(index, index + 1)

    # compute maximum number of overlapping live intervals of addresses
    def compute_num_of_overlapping_intervals(self):
        self._compute_live_ranges()
        max_num_of_overlapping_intervals = 0
        current_num_of_overlapping_intervals = 0
        for boundary in self.interval_boundaries:
            if boundary.is_start:
                current_num_of_overlapping_intervals += 1
            else:
                current_num_of_overlapping_intervals -= 1
            max_num_of_overlapping_intervals = max(max_num_of_overlapping_intervals,
                                                   current_num_of_overlapping_intervals)

        self.max_num_of_overlapping_intervals = max_num_of_overlapping_intervals


class VertexState(Enum):
    UNVISITED = auto()
    VISITED = auto()
    ON_STACK = auto()


@dataclass
class TarjanVertex:
    num: int
    low_link: int = -1
    dfs_index: int = -1
    state: VertexState = VertexState.UNVISITED


# finds strongly connected components in TraceGraph
class StronglyConnectedComponents:
    # Tarjan's algorithm

    def __init__(self, trace_graph: TraceGraph):
        self._trace_graph = trace_graph
        self._current_index = 0
        self._stack = list()
        self._tarjan_vertices = list()
        for i in range(self._trace_graph.num_of_vertices):
            self._tarjan_vertices.append(TarjanVertex(i))
        self._strongly_connected_components = list()

    def _tarjan_dfs(self, vertex: TarjanVertex):
        self._stack.append(vertex)
        vertex.state = VertexState.ON_STACK
        vertex.dfs_index = self._current_index
        self._current_index += 1

        # compute low link
        vertex.low_link = vertex.dfs_index
        for neighbour in self._trace_graph.adjacency[vertex.num]:
            neighbour_vertex = self._tarjan_vertices[neighbour]
            if neighbour_vertex.state == VertexState.VISITED:
                continue
            if neighbour_vertex.state == VertexState.ON_STACK:
                vertex.low_link = min(vertex.low_link, neighbour_vertex.dfs_index)
                continue

            self._tarjan_dfs(neighbour_vertex)
            vertex.low_link = min(neighbour_vertex.low_link, vertex.low_link)

        if vertex.low_link == vertex.dfs_index:
            self._strongly_connected_components.append(list())
            strongly_connected_component_vertex = self._stack.pop()
            strongly_connected_component_vertex.state = VertexState.VISITED

            while strongly_connected_component_vertex.num != vertex.num:
                self._strongly_connected_components[-1].append(strongly_connected_component_vertex)
                strongly_connected_component_vertex = self._stack.pop()
                strongly_connected_component_vertex.state = VertexState.VISITED
            self._strongly_connected_components[-1].append(strongly_connected_component_vertex)

    def compute_strongly_connected_components(self):
        for vertex in self._tarjan_vertices:
            if vertex.state == VertexState.UNVISITED:
                self._tarjan_dfs(vertex)

    @property
    def strongly_connected_components(self):
        strongly_connected_components_nums = list()
        for strongly_connected_component in self._strongly_connected_components:
            strongly_connected_components_nums.append(list())
            for vertex in strongly_connected_component:
                strongly_connected_components_nums[-1].append(vertex.num)
        return strongly_connected_components_nums


# class dividing addresses into groups of: only read from, only written to and read before written addresses
class ProblematicAddresses:
    def __init__(self, pinatrace_input: PinatraceInput):
        self._pinatrace_input = pinatrace_input
        self.only_written_to = list()
        self.only_read_from = list()
        self.read_before_write = list()
        self._find_problematic_addresses()

    def _find_problematic_addresses(self):
        address_operations = defaultdict(list)
        for line in self._pinatrace_input.lines:
            address_operations[line.address].append(line.operation_type)

        for address in address_operations:
            if len(set(address_operations[address])) == 1:
                if address_operations[address][0] == 'R':
                    self.only_read_from.append(address)
                elif address_operations[address][0] == 'W':
                    self.only_written_to.append(address)
            else:
                if address_operations[address][0] == 'R':
                    self.read_before_write.append(address)


# class compressing trace graph
class TraceGraphCompression:
    def __init__(self, trace_graph: TraceGraph):
        self._graph = trace_graph
        self._visited = [False] * self._graph.num_of_vertices
        self._vertices_to_compress = []  # last value is edge weight
        self._current_edge_weight = -1
        self._current_vertices = []
        self.num_of_compressed_vertices = 0
        self.compressed_vertices = set()

    def _compress_vertices(self, vertices: list, new_edge_weight):
        for i in range(len(vertices) - 1):
            vertex_from = vertices[i]
            vertex_to = vertices[i + 1]
            if not self._graph.exists_edge(vertex_from, vertex_to):
                return False

            self.compressed_vertices.add(vertices[i])
            self.compressed_vertices.add(vertices[i + 1])

        for i in range(len(vertices) - 1):
            vertex_from = vertices[i]
            vertex_to = vertices[i + 1]
            del self._graph.adjacency[vertex_from][vertex_to]
        self._graph.add_edge(vertices[0], vertices[-1], new_edge_weight)
        self.num_of_compressed_vertices += len(vertices)
        return True

    def _compress_dfs(self, vertex):
        self._visited[vertex] = True
        if len(self._graph.adjacency[vertex]) > 1 or len(self._graph.adjacency[vertex]) == 0:
            if len(self._current_vertices) > 1:
                self._current_vertices.append(self._current_edge_weight)
                self._vertices_to_compress.append(self._current_vertices[:])
                self._current_vertices.clear()
                self._current_edge_weight = -1

        for neighbour in self._graph.adjacency[vertex]:
            if not self._visited[neighbour]:
                if self._current_edge_weight == -1:
                    self._current_edge_weight = self._graph.adjacency[vertex][neighbour]
                elif self._current_edge_weight == self._graph.adjacency[vertex][neighbour]:
                    self._current_vertices.append(neighbour)
                else:
                    if len(self._current_vertices) > 1:
                        self._current_vertices.append(self._current_edge_weight)
                        self._vertices_to_compress.append(self._current_vertices[:])
                        self._current_vertices.clear()
                        self._current_edge_weight = -1

                self._compress_dfs(neighbour)
                self._current_edge_weight = -1
                self._current_vertices.clear()

    def compress_graph(self):
        self._compress_dfs(self._graph.first_vertex_int)
        for vertices_to_compress in self._vertices_to_compress:
            if vertices_to_compress:
                edge_weight = vertices_to_compress.pop()
                self._compress_vertices(vertices_to_compress, edge_weight)


def branching_factor(trace_input: TraceInput):
    graph = TraceGraph(trace_input)
    number_of_branches = dict()
    for neighbours in graph.adjacency:
        number_of_branches[len(neighbours)] = number_of_branches.get(len(neighbours), 0) + 1
    return number_of_branches
