from collections import defaultdict

import matplotlib.pyplot as plt

from .analyses import OverlappingIntervals, ProblematicAddresses
from .data_structures import TraceInput, MemoryMaps


# plot addresses in graph, addresses in subsequences are colored red
def plot_addresses(subsequences, trace_input: TraceInput):
    subsequences.sort(key=lambda sequence: sequence.end)
    color_array = ['b'] * len(trace_input.addresses)
    if len(subsequences) != 0:
        subsequence_index = 0
        subsequence_start = subsequences[subsequence_index].end - subsequences[subsequence_index].length
        subsequence_end = subsequences[subsequence_index].end

        for index, address in enumerate(trace_input.addresses):
            if subsequence_start <= index < subsequence_end:
                color_array[index] = 'r'
            elif subsequence_end <= index:
                if subsequence_index + 1 < len(subsequences):
                    subsequence_index += 1
                    subsequence_start = subsequences[subsequence_index].end - subsequences[subsequence_index].length
                    subsequence_end = subsequences[subsequence_index].end
                    if subsequence_start <= index < subsequence_end:
                        color_array[index] = 'r'
                else:
                    continue

    plt.scatter([x for x in range(len(trace_input.addresses))], trace_input.addresses, s=0.1, c=color_array)

    # plt.ylabel('Adresy')
    # plt.xlabel('Počet prístupov')
    plt.ylabel('Addresses')
    plt.xlabel('Number of accesses')
    plt.yticks([])
    plt.show()


# plot percentage of addresses needed, if relative is set to True,
# percentage is computed based on number of addresses accessed until that moment
# otherwise percentage is computed based on number of all addresses accessed
def plot_percentage_of_addresses_needed(trace_input: TraceInput, relative=True):
    overlapping_intervals = OverlappingIntervals(trace_input)
    percentages_of_addresses_needed = list()
    current_addresses_set = set()
    interval_boundary_index = 0
    current_boundary = overlapping_intervals.interval_boundaries[interval_boundary_index]
    current_num_of_intervals = 0
    passed_all_boundaries = False

    for index, address in enumerate(trace_input.addresses):
        current_addresses_set.add(address)
        if not passed_all_boundaries and current_boundary.boundary_index == index:
            while current_boundary.boundary_index == index:
                if current_boundary.is_start:
                    current_num_of_intervals += 1
                else:
                    current_num_of_intervals -= 1
                interval_boundary_index += 1
                if interval_boundary_index >= len(overlapping_intervals.interval_boundaries):
                    passed_all_boundaries = True
                else:
                    current_boundary = overlapping_intervals.interval_boundaries[interval_boundary_index]

        else:
            current_boundary = overlapping_intervals.interval_boundaries[interval_boundary_index]
        if relative:
            percentages_of_addresses_needed.append((current_num_of_intervals / len(current_addresses_set)) * 100)
        else:
            percentages_of_addresses_needed.append((current_num_of_intervals / trace_input.num_of_addresses) * 100)

    plt.xlabel("Number Of Accesses")
    plt.ylabel("Percentage")
    plt.scatter([x for x in range(len(percentages_of_addresses_needed))], percentages_of_addresses_needed, s=0.1)
    plt.show()


# return percentage of addresses needed
def percentage_of_addresses_needed(trace_input: TraceInput):
    overlapping_intervals = OverlappingIntervals(trace_input)
    overlapping_intervals.compute_num_of_overlapping_intervals()
    # print(overlapping_intervals.max_num_of_overlapping_intervals, len(trace_input.addresses))
    return str((overlapping_intervals.max_num_of_overlapping_intervals / len(set(trace_input.addresses))) * 100)


def percentage_of_problematic_addresses_mapped(problematic_addresses: ProblematicAddresses, memory_maps: MemoryMaps):
    mapped_only_written_to = 0

    for address_only_written_to in problematic_addresses.only_written_to:
        address_only_written_to = int(address_only_written_to, 16)
        for interval in memory_maps.intervals:
            if interval.start_address <= address_only_written_to < interval.end_address:
                mapped_only_written_to += 1
                break

    mapped_only_read_from = 0

    for address_only_read_from in problematic_addresses.only_read_from:
        address_only_read_from = int(address_only_read_from, 16)
        for interval in memory_maps.intervals:
            if interval.start_address <= address_only_read_from < interval.end_address:
                mapped_only_read_from += 1
                break

    mapped_read_before_write = 0

    for address_read_before_write in problematic_addresses.read_before_write:
        address_read_before_write = int(address_read_before_write, 16)
        for interval in memory_maps.intervals:
            if interval.start_address <= address_read_before_write < interval.end_address:
                mapped_read_before_write += 1
                break
    return {
        'mapped_only_written_to': (mapped_only_written_to / len(problematic_addresses.only_written_to)) * 100,
        'mapped_only_read_from': (mapped_only_read_from / len(problematic_addresses.only_read_from)) * 100,
        'mapped_read_before_write': (mapped_read_before_write / len(problematic_addresses.read_before_write)) * 100
    }


# show pie charts based on ProblematicAddresses groups of addresses
def pie_chart_problematic_addresses(problematic_addresses: ProblematicAddresses, num_of_all_addresses):
    labels = "Addresses only read from", "Addresses only written to", "Addresses read before written", "Other addresses"
    # labels_sk = "Iba čítané", "Iba zapisované", "Čítané pred zápisom", "Ostatné"
    other_addresses = num_of_all_addresses - (len(problematic_addresses.only_read_from) + len(
        problematic_addresses.only_written_to) + len(problematic_addresses.read_before_write))
    sizes = [len(problematic_addresses.only_read_from) / num_of_all_addresses * 100,
             len(problematic_addresses.only_written_to) / num_of_all_addresses * 100,
             len(problematic_addresses.read_before_write) / num_of_all_addresses * 100,
             other_addresses / num_of_all_addresses * 100
             ]
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.0f%%')
    ax1.axis('equal')

    # plt.savefig(f"{filename}.png")

    # plt.close(fig1)
    plt.show()


# show bar graph with length of subsequences from sequential or spatial pattern
def subsequences_bar_graph(subsequences):
    num_of_subsequences_length = defaultdict(int)
    for subsequence in subsequences:
        num_of_subsequences_length[subsequence.length] += 1
    subsequences_lengths = sorted(num_of_subsequences_length)
    plt.bar(subsequences_lengths,
            [num_of_subsequences_length[subsequences_length] for subsequences_length in subsequences_lengths])
    # plt.xlabel('Dĺžka podpostupnosti')
    # plt.ylabel('Počet')
    plt.xlabel('Subsequence length')
    plt.ylabel('Count')
    plt.show()
