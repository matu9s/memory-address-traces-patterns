from dataclasses import dataclass

import networkx as nx


# Input files lines

# Pin
@dataclass
class PinatraceInputLine:
    instruction_address: str
    operation_type: str
    address: str

    def __repr__(self):
        return f'0x{self.instruction_address}: {self.operation_type} 0x{self.address}'


# Pin
@dataclass
class InstraceInputLine:
    address: str
    operation_code: str

    def __repr__(self):
        return f'0x{self.address}, {self.operation_code}'


# Dynamorio
@dataclass
class MemtraceInputLine:
    address: str
    data_size: str
    operation_type: str

    def __repr__(self):
        return f'0x{self.address}: {self.data_size}, {self.operation_type}'


# Input files classes

class TraceInput:
    def __init__(self, input_file_name):
        self.addresses = list()
        self._trace_file_name = input_file_name
        self.num_of_addresses = 0


# Pin pinatrace file
class PinatraceInput(TraceInput):
    def __init__(self, input_file_name):
        super().__init__(input_file_name)
        self.lines = list()
        self._read_input_file()
        self.num_of_addresses = len(set(self.addresses))

    def _read_input_file(self):
        with open(self._trace_file_name) as trace_file:
            line = trace_file.readline()
            while line:
                pinatrace_input_line = PinatraceInputLine(line.split()[0][2:-1], line.split()[1], line.split()[2][2:])
                self.lines.append(pinatrace_input_line)
                self.addresses.append(pinatrace_input_line.address)
                line = trace_file.readline()
                if line.strip() == '#eof':
                    break


# Pin itrace file
class ITraceInput(TraceInput):
    def __init__(self, input_file_name):
        super().__init__(input_file_name)
        self._read_input_file()
        self.num_of_addresses = len(set(self.addresses))

    def _read_input_file(self):
        with open(self._trace_file_name) as trace_file:
            address = trace_file.readline().strip()
            while address:
                self.addresses.append(address[2:])
                address = trace_file.readline().strip()
                if address.strip() == '#eof':
                    break


# Dynamorio memtrace file
class MemtraceInput(TraceInput):
    def __init__(self, input_file_name):
        super().__init__(input_file_name)
        self.lines = list()
        self._read_input_file()
        self.num_of_addresses = len(set(self.addresses))

    def _read_input_file(self):
        with open(self._trace_file_name) as trace_file:
            trace_file.readline()
            line = trace_file.readline()
            while line:
                memtrace_input_line = MemtraceInputLine(line.split()[0][2:-1], line.split()[1][:-1],
                                                        line.split()[2][2:])
                self.lines.append(memtrace_input_line)
                self.addresses.append(memtrace_input_line.address)
                line = trace_file.readline()


# Dynamorio instrace file
class InstraceInput(TraceInput):
    def __init__(self, input_file_name):
        super().__init__(input_file_name)
        self.lines = list()
        self._read_input_file()
        self.num_of_addresses = len(set(self.addresses))

    def _read_input_file(self):
        with open(self._trace_file_name) as trace_file:
            trace_file.readline()
            line = trace_file.readline()
            while line:
                instrace_input_line = InstraceInputLine(line.split(',')[0][2:], line.split(',')[1])
                self.lines.append(instrace_input_line)
                self.addresses.append(instrace_input_line.address)
                line = trace_file.readline()


# Graph created from list of accessed addresses
class TraceGraph:
    def __init__(self, trace_input: TraceInput):
        self._trace_input = trace_input
        self.adjacency = list()
        self.address_to_int = dict()
        self.int_to_address = dict()
        self.num_of_vertices = len(set(self._trace_input.addresses))
        self.first_vertex_int = int
        self._set_up_graph()

    def _set_up_graph(self):
        # create mapping between address and integer from 0 to number of addresses
        for i, address in enumerate(sorted(set(self._trace_input.addresses))):
            self.address_to_int[address] = i
            self.int_to_address[i] = address
            self.adjacency.append(dict())

        self.first_vertex_int = self.address_to_int[self._trace_input.addresses[0]]

        # create graph
        previous_address_int = None
        for address in self._trace_input.addresses:
            address_int = self.address_to_int[address]
            if previous_address_int is not None:
                self.add_edge(previous_address_int, address_int)
            previous_address_int = address_int

    def add_edge(self, int_from, int_to, weight=1):
        self.adjacency[int_from][int_to] = self.adjacency[int_from].get(int_to, 0) + weight

    def exists_edge(self, int_from, int_to):
        return int_to in self.adjacency[int_from]

    # returns nx Graph, can be used for other graph algorithms
    def get_digraph(self):
        digraph_dict = dict()
        for vertex, neighbours in enumerate(self.adjacency):
            digraph_dict[str(vertex)] = list()
            for neighbour in neighbours:
                digraph_dict[str(vertex)].append(str(neighbour))
        return nx.DiGraph(digraph_dict)


@dataclass
class MapInterval:
    start_address: int = None
    end_address: int = None
    permissions: str = None
    offset: int = None
    device: str = None
    inode: int = None
    pathname: str = None


# class representing /proc/self/maps file
class MemoryMaps:
    def __init__(self, input_file_name):
        self._input_file_name = input_file_name
        self.intervals = list()
        self._read_input_file()

    def _read_input_file(self):
        with open(self._input_file_name) as input_file:
            for line in input_file:
                split_line = line.split()

                map_interval = MapInterval()
                map_interval.start_address = int(split_line[0].split('-')[0], 16)
                map_interval.end_address = int(split_line[0].split('-')[1], 16)
                map_interval.permissions = split_line[1]
                map_interval.offset = split_line[2]
                map_interval.device = split_line[3]
                map_interval.inode = split_line[4]

                if len(split_line) == 6:
                    map_interval.pathname = split_line[5]

                self.intervals.append(map_interval)
